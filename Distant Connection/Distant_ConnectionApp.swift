//
//  Distant_ConnectionApp.swift
//  Distant Connection
//
//  Created by Jonathan Alexander on 2/20/24.
//

import SwiftUI

@main
struct Distant_ConnectionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
